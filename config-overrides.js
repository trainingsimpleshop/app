const { addLessLoader, fixBabelImports, override } = require("customize-cra");
module.exports = override(
  fixBabelImports('antd', {
    libraryDirectory: 'es',
    style: true,
  }),
  addLessLoader({
    javascriptEnabled: true
  })
);