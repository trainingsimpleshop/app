import React from 'react'
import { Result } from 'antd'

const NotFoundPage = () => {
  return (
    <div>
      <Result status="404"/>
    </div>
  )
}

export default NotFoundPage
