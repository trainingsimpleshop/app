import React, { useEffect, useState } from 'react'
import { useAuth } from '../component/AuthProvider'
import Axios from 'axios'
import { Typography, Table, List, Skeleton, Row, Col } from 'antd'
import _ from 'lodash'
import { useHistory } from 'react-router-dom'

const MyOrderPage = () => {
  const {
    token
  } = useAuth()
  const history = useHistory()
  const [orders, setorders] = useState([])
  useEffect(() => {
    const getOrders = async () => {
      const result = await Axios.get('http://localhost:3000/api/appusers/me/orders', {
        headers: {
          Authorization: token
        },
        params: {
          filter: JSON.stringify({
            order: 'id desc',
            where: {
              status: {
                neq: 'cart'
              }
            },
            include: {
              orderItems: ['product']
            }
          })
        }
      }).then(res => res.data)
      setorders(result)
    }
    getOrders()
  }, [token])
  const columns = [
    {
      title: 'Order ID',
      dataIndex: 'id',
      key: 'id',
    },
    {
      title: 'Items',
      dataIndex: 'orderItems',
      key: 'orderItems',
      render: orderItems => (
        <List
          className="demo-loadmore-list"
          itemLayout="horizontal"
          dataSource={orderItems}
          renderItem={item => (
            <List.Item>
              <Skeleton avatar title={false} loading={item.loading} active>
                <List.Item.Meta
                  title={<a href="https://ant.design">{_.get(item, 'product.name', 'Unknown product')}</a>}
                  description={`${_.get(item, 'product.price', 0)} Baht`}
                />
                <Row justify="end" gutter={40}>
                  <Col>
                    x {item.quantity}
                  </Col>
                </Row>
              </Skeleton>
            </List.Item>
          )} 
        />
      ),
    },
    {
      title: 'Grand Total',
      dataIndex: 'grandTotal',
      key: 'grandTotal',
    },
    {
      title: 'Status',
      dataIndex: 'status',
      key: 'status',
    },
  ]
  return (
    <div>
      <Typography.Title level={3}>
        My orders
      </Typography.Title>
      <Table dataSource={orders} columns={columns} onRow={(record) => {
        return {
          onClick: () => history.push(`/order/${record.id}`), // click row
        };
      }}/>;
    </div>
  )
}

export default MyOrderPage
