import React from 'react'
import { Card, Avatar } from 'antd'
import { ShoppingCartOutlined } from '@ant-design/icons'
import Axios from 'axios';
import { useAuth } from '../../component/AuthProvider';
import { useCart } from '../../component/CartProvider';

const { Meta } = Card;
const ProductCard = ({
  data
}) => {
  const {
    token
  } = useAuth()
  const {
    reloadCart
  } = useCart()
  const handleAddToCart = async () => {
    const { id } = data
    // add to cart with product id
    const payload = {
      productId: id,
      quantity: 1
    }
    debugger
    const carts = await Axios.get("http://localhost:3000/api/appusers/me/orders", {
      params: {
        filter: JSON.stringify({
          where: {
            status: 'cart'
          }
        })
      },
      headers: {
        Authorization: token
      }
    }).then(res => res.data)
    // check cart exist
    let cart = carts[0]
    if (!cart) {
      // cart not exist
      const createdCart = await Axios.post("http://localhost:3000/api/appusers/me/orders", {}).then(res => res.data)
      cart = createdCart
    }
    // create orderItem
    const createdOrderItem = await Axios.post(`http://localhost:3000/api/orders/${cart.id}/orderItems`, payload).then(res => res.data)
    // show popup -> Product added to cart
    reloadCart()
  }
  return (
    <Card
      style={{ width: 300 }}
      cover={
        <img
          alt="example"
          src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
        />
      }
      actions={[
        <ShoppingCartOutlined key="setting" onClick={handleAddToCart}/>,
      ]}
    >
      <Meta
        title={data.name}
        description={data.description}
      />
    </Card>
  )
}

export default ProductCard
