import React from 'react'
import { Row, Col } from 'antd'
import ItemList from './CartPage/ItemList'
import CartSummary from './CartPage/CartSummary'

const CartPage = () => {
  return (
    <Row>
      <Col span={12}>
        <ItemList />
      </Col>
      <Col span={12}>
        <CartSummary />
      </Col>
    </Row>
  )
}

export default CartPage
