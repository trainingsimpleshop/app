import React from 'react'
import { useCart } from '../../component/CartProvider'
import { Typography, List, Skeleton, Avatar, Row, Col, InputNumber } from 'antd'
import _ from 'lodash'
import { DeleteOutlined } from '@ant-design/icons'
import Axios from 'axios'

const ItemList = () => {
  const {
    cart,
    reloadCart
  } = useCart()
  const {
    orderItems = []
  } = cart || {}
  const handleDelete = async (orderItem) => {
    const {
      orderId,
      id: orderItemId
    } = orderItem
    await Axios.delete(`http://localhost:3000/api/orders/${orderId}/orderItems/${orderItemId}`)
    reloadCart()
  }
  const handleQuantityChange = async (quantity, orderItem) => {
    const {
      id: orderItemId
    } = orderItem
    await Axios.patch(`http://localhost:3000/api/orderItems/${orderItemId}`, {
      quantity
    })
    reloadCart()
  }
  return (
    <div>
      <Typography.Title level={3}>
        Items
      </Typography.Title>
      <div>
        <List
          className="demo-loadmore-list"
          itemLayout="horizontal"
          dataSource={orderItems}
          renderItem={item => (
            <List.Item
              actions={[<DeleteOutlined onClick={() => handleDelete(item)}/>]}
            >
              <Skeleton avatar title={false} loading={item.loading} active>
                <List.Item.Meta
                  avatar={
                    <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                  }
                  title={<a href="https://ant.design">{_.get(item, 'product.name', 'Unknown product')}</a>}
                  description={`${_.get(item, 'product.price', 0)} Baht`}
                />
                <Row justify="end">
                  <Col>
                    <InputNumber defaultValue={item.quantity} onChange={(value) => handleQuantityChange(value, item)} />
                  </Col>
                  <Col>
                    {item.quantity * _.get(item, 'product.price', 0)} Baht
                  </Col>
                </Row>
              </Skeleton>
            </List.Item>
          )}
        />
      </div>
    </div>
  )
}

export default ItemList
