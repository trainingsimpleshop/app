import React, { useState } from 'react'
import { Typography, List, Row, Col, Skeleton, Input, Button } from 'antd'
import { useCart } from '../../component/CartProvider'
import { EditOutlined, SaveOutlined } from '@ant-design/icons'
import Axios from 'axios'
import _ from 'lodash'
import { useHistory } from 'react-router-dom'

const CartSummary = () => {
  const {
    cart,
    reloadCart
  } = useCart()
  const history = useHistory()
  const [editShippingAddress, seteditShippingAddress] = useState(false)
  const [shippingAddress, setshippingAddress] = useState(_.get(cart, 'shippingAddress', ''))
  if (!cart) {
    return <Skeleton />
  }
  const setAddressEditMode = () => {
    seteditShippingAddress(true)
  }
  const handleSaveAddress = async () => {
    // api update order address
    await Axios.patch(`http://localhost:3000/api/orders/${cart.id}`, {
      shippingAddress
    })
    // reload
    reloadCart()
    seteditShippingAddress(false)
  }
  const handleAddressChange = (e) => {
    debugger
    setshippingAddress(e.target.value)
  }
  const handleCheckout = async () => {
    const currentCart = await Axios.post(`http://localhost:3000/api/orders/${cart.id}/checkout`).then(res => res.data)
    reloadCart()
    history.push(`/order/${currentCart.id}`)
  }
  return (
    <div>
      <Typography.Title level={3}>
        Cart Total
      </Typography.Title>
      <List>
        <List.Item>
          <List.Item.Meta
            title='Subtotal'
          />
          <Row justify="end">
            <Col>
              {cart.totalAmount || 0} Baht
            </Col>
          </Row>
        </List.Item>
        <List.Item
          actions={[
            editShippingAddress ? <SaveOutlined onClick={handleSaveAddress}/> : <EditOutlined onClick={setAddressEditMode}/>
          ]}
        >
          <List.Item.Meta
            title='Shipping Address'
            description={cart.shippingAddress}
          />
          {editShippingAddress &&
            <Input.TextArea onChange={handleAddressChange}/>
          }
        </List.Item>
        <List.Item>
          <List.Item.Meta
            title='Shipping Cost'
          />
          <Row justify="end">
            <Col>
              {cart.shippingCost || 0} Baht
            </Col>
          </Row>
        </List.Item>
        <List.Item>
          <List.Item.Meta
            title='Grand total'
          />
          <Row justify="end">
            <Col>
              {cart.grandTotal || 0} Baht
            </Col>
          </Row>
        </List.Item>
        <List.Item>
          <List.Item.Meta
            title='Order status'
          />
          <Row justify="end">
            <Col>
              {cart.status}
            </Col>
          </Row>
        </List.Item>
      </List>
      <Button onClick={handleCheckout}>
        Checkout
      </Button>
    </div>
  )
}

export default CartSummary
