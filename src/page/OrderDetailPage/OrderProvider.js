import React, { useState, useContext, useEffect } from 'react'
import Axios from 'axios'
import { useAuth } from '../../component/AuthProvider'

const Context = React.createContext()
const OrderProvider = ({
  children,
  orderId
}) => {
  const [order, setorder] = useState()
  const [timestamp, settimestamp] = useState(Date.now())
  const {
    token
  } = useAuth()
  useEffect(() => {
    const getOrder = async () => {
      const orderResult = await Axios.get(`http://localhost:3000/api/orders/${orderId}`, {
        params: {
          filter: JSON.stringify({
            include: {
              orderItems: ['product']
            }
          }),
        },
        headers: {
          Authorization: token
        }
      }).then(res => res.data)
      setorder(orderResult)
    }
    getOrder()
  }, [token, timestamp, orderId])
  // api get current cart
  const reloadOrder = () => {
    const currentTimestamp = Date.now()
    settimestamp(currentTimestamp)
  }
  return (
    <Context.Provider value={{
      order,
      reloadOrder
    }}>
      {children}
    </Context.Provider>
  )
}

export const useOrder = () => {
  const context = useContext(Context)
  if (!context) {
    throw new Error('Cannot use useOrder outside order provider')
  }
  return context
}

export default OrderProvider
