import React, { useState, useEffect } from 'react'
import { Typography, List, Row, Col, Skeleton, Input, Button } from 'antd'
import { useCart } from '../../component/CartProvider'
import { EditOutlined, SaveOutlined } from '@ant-design/icons'
import Axios from 'axios'
import _ from 'lodash'
import { useHistory } from 'react-router-dom'
import UploadPaymentReference from './UploadPaymentReference'
import { useOrder } from './OrderProvider'

const CartSummary = ({
  orderId
}) => {
  const {
    order
  } = useOrder()
  const history = useHistory()
  if (!order) {
    return <Skeleton />
  }
  return (
    <div>
      <Typography.Title level={3}>
        Order Total
      </Typography.Title>
      <List>
        <List.Item>
          <List.Item.Meta
            title='Subtotal'
          />
          <Row justify="end">
            <Col>
              {order.totalAmount || 0} Baht
            </Col>
          </Row>
        </List.Item>
        <List.Item
        >
          <List.Item.Meta
            title='Shipping Address'
            description={order.shippingAddress}
          />
        </List.Item>
        <List.Item>
          <List.Item.Meta
            title='Shipping Cost'
          />
          <Row justify="end">
            <Col>
              {order.shippingCost || 0} Baht
            </Col>
          </Row>
        </List.Item>
        <List.Item>
          <List.Item.Meta
            title='Grand total'
          />
          <Row justify="end">
            <Col>
              {order.grandTotal || 0} Baht
            </Col>
          </Row>
        </List.Item>
        <List.Item>
          <List.Item.Meta
            title='Order status'
          />
          <Row justify="end">
            <Col>
              {order.status}
            </Col>
          </Row>
        </List.Item>
        <List.Item>
          <List.Item.Meta
            title='Payment reference'
          />
          <Row justify="end">
            <Col style={{padding: 20}}>
              <img style={{width: '100%'}} src={order.paymentReference} alt="reference" />
            </Col>
          </Row>
        </List.Item>
        {
          (order.status === 'pendingPayment' || order.status === 'reject') &&
          <UploadPaymentReference orderId={orderId}/>
        }
      </List>
    </div>
  )
}

export default CartSummary
