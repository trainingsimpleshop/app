import React from 'react'
import { Typography, List, Skeleton, Avatar, Row, Col } from 'antd'
import _ from 'lodash'
import { useOrder } from './OrderProvider'

const ItemList = () => {
  const {
    order
  } = useOrder()
  
   // fetch data by orderid
  const {
    orderItems = []
  } = order || {}
  return (
    <div>
      <Typography.Title level={3}>
        Items
      </Typography.Title>
      <div>
        <List
          className="demo-loadmore-list"
          itemLayout="horizontal"
          dataSource={orderItems}
          renderItem={item => (
            <List.Item
            >
              <Skeleton avatar title={false} loading={item.loading} active>
                <List.Item.Meta
                  avatar={
                    <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                  }
                  title={<a href="https://ant.design">{_.get(item, 'product.name', 'Unknown product')}</a>}
                  description={`${_.get(item, 'product.price', 0)} Baht`}
                />
                <Row justify="end" gutter={40}>
                  <Col>
                    x {item.quantity}
                  </Col>
                  <Col>
                    {item.quantity * _.get(item, 'product.price', 0)} Baht
                  </Col>
                </Row>
              </Skeleton>
            </List.Item>
          )}
        />
      </div>
    </div>
  )
}

export default ItemList
