import React, { useState } from 'react'
import { Upload, message, Button } from 'antd';
import { InboxOutlined } from '@ant-design/icons';
import _ from 'lodash'
import Axios from 'axios';
import { useOrder } from './OrderProvider';

const { Dragger } = Upload;

const UploadPaymentReference = () => {
  const [fileUrl, setfileUrl] = useState()
  const {
    order,
    reloadOrder
  } = useOrder()
  const props = {
    name: 'file',
    multiple: true,
    action: 'http://localhost:3000/api/containers/upload/upload',
    onChange(info) {
      const { status, response } = info.file;
      if (status !== 'uploading') {
        console.log(info.file, info.fileList);
      }
      if (status === 'done') {
        debugger
        message.success(`${info.file.name} file uploaded successfully.`);
        // create download url
        const filename = _.get(response, 'result.files.file[0].name');
        if (!filename) {
          message.error(`Cannot find file name, Something went wrong`);
        }
        const fileUrl = `http://localhost:3000/api/containers/upload/download/${filename}`
        setfileUrl(fileUrl)
        // save file to state
      } else if (status === 'error') {
        message.error(`${info.file.name} file upload failed.`);
      }
    },
  };
  const handleSubmitPayment = async () => {
    try {
      await Axios.post(`http://localhost:3000/api/orders/${order.id}/verify`, {
        paymentReference: fileUrl
      })
      reloadOrder()
      message.success('Payment verify submitted')
    } catch (e) {
      message.error('Payment verify fail, something went wrong')
      // logger
    }
  }
  return (
    <React.Fragment>
      <Dragger {...props}>
        {fileUrl ? <img src={fileUrl} alt="preview" style={{width: '100%'}}/> : (
          <React.Fragment>
            <p className="ant-upload-drag-icon">
              <InboxOutlined />
            </p>
            <p className="ant-upload-text">Click or drag file to this area to upload</p>
            <p className="ant-upload-hint">
              Support for a single or bulk upload. Strictly prohibit from uploading company data or other
              band files
            </p>
          </React.Fragment>
        )}
    </Dragger>
    <Button onClick={handleSubmitPayment}>Submit Payment Reference</Button>
  </React.Fragment>
  )
}

export default UploadPaymentReference
