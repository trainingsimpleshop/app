import React from 'react'
import { useRouteMatch } from 'react-router-dom'
import { Result, Row, Col } from 'antd'
import ItemList from './OrderDetailPage/ItemList'
import CartSummary from './OrderDetailPage/CartSummary'
import OrderProvider from './OrderDetailPage/OrderProvider'

const OrderDetailPage = () => {
  const {
    params
  } = useRouteMatch()
  const {
    orderId
  } = params
  if (!orderId) {
    return <Result status="404"/>
  }
  return (
    <OrderProvider orderId={orderId}>
      <Row>
        <Col span={12}>
          <ItemList orderId={orderId} />
        </Col>
        <Col span={12}>
          <CartSummary orderId={orderId} />
        </Col>
      </Row>
    </OrderProvider>
  )
}

export default OrderDetailPage
