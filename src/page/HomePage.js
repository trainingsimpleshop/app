import React, { useEffect, useState } from 'react'
import { Typography, Row, Col } from 'antd'
import ProductCard from './HomePage/ProductCard'
import Axios from 'axios'

const HomePage = () => {
  const [productDatas, setproductDatas] = useState([])
  useEffect(() => {
    const listProduct = async () => {
      const result = await Axios.get('http://localhost:3000/api/products').then(res => res.data)
      setproductDatas(result)
    }
    listProduct()
  }, [])
  // const productDatas = [
  //   {
  //     id: 1,
  //     name: 'Product 1',
  //     description: 'This is test product',
  //     price: 300
  //   },
  //   {
  //     id: 2,
  //     name: 'Product 2',
  //     description: 'This is test product',
  //     price: 150
  //   },
  //   {
  //     id: 3,
  //     name: 'Product 3',
  //     description: 'This is test product',
  //     price: 180
  //   },
  //   {
  //     id: 4,
  //     name: 'Product 4',
  //     description: 'This is test product',
  //     price: 400
  //   },
  //   {
  //     id: 5,
  //     name: 'Product 5',
  //     description: 'This is test product',
  //     price: 430
  //   }
  // ]
  return (
    <div>
      <Typography.Title>Products</Typography.Title>
      <Row>
        {productDatas.map(productData => {
          return (
            <Col span={6}>
              <ProductCard data={productData}/>
            </Col>
          )
        })}
      </Row>
    </div>
  )
}

export default HomePage
