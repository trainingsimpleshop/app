import React, { useEffect, useState } from 'react'
import Axios from 'axios'
import { useAuth } from '../component/AuthProvider'

const ProfilePage = () => {
  const [profile, setprofile] = useState()
  const {
    token
  } = useAuth()
  useEffect(() => {
    const getProfile = async () => {
      if (!token) {
        setprofile(undefined)
        return
      }
      const profileResponse = await Axios.get('http://localhost:3000/api/appusers/me', {
        headers: {
          Authorization: token
        }
      }).then(res => res.data)
      console.log(profileResponse)
      setprofile(profileResponse)
    }
    getProfile()
    return () => {}
  }, [token])
  return (
    <div>
      Profile page
      {JSON.stringify(profile)}
    </div>
  )
}

export default ProfilePage
