import React, { useState, useContext, useEffect } from 'react'
import Axios from 'axios'
import { useAuth } from './AuthProvider'

const Context = React.createContext()
const CartProvider = ({
  children
}) => {
  const [cart, setcart] = useState()
  const [timestamp, settimestamp] = useState(Date.now())
  const {
    token
  } = useAuth()
  useEffect(() => {
    const getCurrentCart = async () => {
      const carts = await Axios.get("http://localhost:3000/api/appusers/me/orders", {
        params: {
          filter: JSON.stringify({
            where: {
              status: 'cart'
            },
            include: {
              orderItems: ['product']
            }
          }),
        },
        headers: {
          Authorization: token
        }
      }).then(res => res.data)
      let currentCart = carts[0]
      if (!currentCart) {
        // cart not exist
        const createdCart = await Axios.post("http://localhost:3000/api/appusers/me/orders", {}, {
          headers: {
            Authorization: token
          }
        }).then(res => res.data)
        currentCart = createdCart
      }
      setcart(currentCart)
      debugger
    }
    getCurrentCart()
  }, [token, timestamp])
  // api get current cart
  const reloadCart = () => {
    const currentTimestamp = Date.now()
    settimestamp(currentTimestamp)
  }
  return (
    <Context.Provider value={{
      cart,
      reloadCart
    }}>
      {children}
    </Context.Provider>
  )
}

export const useCart = () => {
  const context = useContext(Context)
  if (!context) {
    throw new Error('Cannot use useCart outside cart provider')
  }
  return context
}

export default CartProvider
