import React, { useState, useContext, useEffect } from 'react'
import Axios from 'axios'
import _ from 'lodash'

const Context = React.createContext()
const AuthProvider = ({
  children
}) => {
  const [token, _settoken] = useState(localStorage.getItem('token'))
  const [user, setuser] = useState()
  useEffect(() => {
    const getUser = async () => {
      if (!token) {
        setuser(undefined)
      }
      const currentUser = await Axios.get('http://localhost:3000/api/appusers/me', {
        params: {
          filter: JSON.stringify({
            include: ['roles']
          })
        },
        headers: {
          Authorization: token
        }
      }).then(res => res.data)
      setuser(currentUser)
    }
    getUser()
  }, [token])
  const setToken = (data) => {
    _settoken(data)
    localStorage.setItem('token', data)
  }
  const clearToken = () => {
    _settoken(undefined)
    localStorage.removeItem('token')
  }
  const checkPermission = (rolesToCheck = []) => {
    const {
      roles = []
    } = user || {}
    const rolesArr = roles.map(role => role.name)
    const haveRole = _.intersection(rolesArr, rolesToCheck).length > 0
    return haveRole
  }
  const isAdmin = checkPermission(['admin'])
  return (
    <Context.Provider value={{
      token,
      setToken,
      clearToken,
      user,
      isAdmin,
      checkPermission
    }}>
      {children}
    </Context.Provider>
  )
}

export const useAuth = () => {
  const context = useContext(Context)
  if (!context) {
    throw new Error('Cannot use useAuth outside auth provider')
  }
  return context
}

export default AuthProvider
