import React from 'react';
import './App.less';
import { Layout } from 'antd';
import AppHeader from './layout/AppHeader';
import { BrowserRouter } from 'react-router-dom';
import AppContent from './layout/AppContent';
import AuthProvider from './component/AuthProvider';
import CartProvider from './component/CartProvider';
function App() {
  return (
    <AuthProvider>
      <CartProvider>
        <BrowserRouter>
          <Layout style={{height: '100vh'}}>
            <Layout.Header>
              <AppHeader />
            </Layout.Header>
            <Layout.Content style={{overflow: 'auto', display: 'flex'}}>
              <AppContent />
            </Layout.Content>
            <Layout.Footer></Layout.Footer>
          </Layout>
        </BrowserRouter>
      </CartProvider>
    </AuthProvider>
  );
}

export default App;
