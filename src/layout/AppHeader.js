import React from 'react'
import { Row, Col, Button, Dropdown, Menu } from 'antd'
import { Link, useHistory } from 'react-router-dom'
import { useAuth } from '../component/AuthProvider'
import { useCart } from '../component/CartProvider'
import { ShoppingCartOutlined } from '@ant-design/icons'
import _ from 'lodash'

const AuthenticatedMenu = () => {
  const {
    clearToken,
    user,
    isAdmin
  } = useAuth()
  const {
    cart
  } = useCart()
  const history = useHistory()
  const cartCount = _.get(cart, 'orderItems.length', 0)
  const menu = (
    <Menu>
      <Menu.Item onClick={() => history.push('/profile')}>
        My profile
      </Menu.Item>
      <Menu.Item onClick={() => history.push('/myorders')}>
        My orders
      </Menu.Item>
      {
        isAdmin &&
          <Menu.Item onClick={() => history.push('/admin')}>
            Manage Store
          </Menu.Item>
      }
      <Menu.Divider />
      <Menu.Item onClick={() => clearToken()}>
        Logout
      </Menu.Item>
    </Menu>
  );
  return (
    <Row>
      <Col>
        <Dropdown overlay={menu} placement="bottomRight">
          <Button>Hi, {_.get(user, 'firstname')}</Button>
        </Dropdown>
      </Col>
      <Col>
        <Button onClick={() => history.push('/cart')}>
          <ShoppingCartOutlined /> {cartCount > 0 && cartCount}
        </Button>
      </Col>
    </Row>
  )
}
const UnauthenticatedMenu = () => {
  return (
    <Row>
      <Col>
        <Link to="/signin">Sign in</Link>
      </Col>
      <Col>
        <Link to="/signup">Sign up</Link>
      </Col>
    </Row>
  )
}
const AppHeader = () => {
  const {
    token
  } = useAuth()
  const history = useHistory()
  const isLoggedIn = !!token
  return (
    <Row justify="space-between">
      <Col>
        <div style={{color: 'white', fontSize: '1.5rem'}} onClick={() => history.push('/')}>
          Simple Shop
        </div>
      </Col>
      <Col>
        {isLoggedIn ? <AuthenticatedMenu /> : <UnauthenticatedMenu />}
      </Col>
    </Row>
  )
}

export default AppHeader
