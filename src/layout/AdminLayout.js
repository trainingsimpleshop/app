import React from 'react'
import { Route, useRouteMatch, Switch, useLocation } from 'react-router-dom'
import DashboardPage from '../page/Admin/DashboardPage'
import ProductManagementPage from '../page/Admin/ProductManagementPage'
import OrderManagementPage from '../page/Admin/OrderManagementPage'
import NotFoundPage from '../page/NotFoundPage'
import { useAuth } from '../component/AuthProvider'
import { Result, Layout, Menu } from 'antd'

const AdminLayout = () => {
  const {
    path
  } = useRouteMatch()
  const {
    pathname
  } = useLocation()
  const {
    isAdmin
  } = useAuth()
  if (!isAdmin) {
    // redirect to forbidden page
    return <Result status="403"/>
  }
  return (
    <Layout>
      <Layout.Sider lig>
        <Menu theme="dark" defaultSelectedKeys={[pathname]}>
          <Menu.Item key={path}>
            Dashboard
          </Menu.Item>
          <Menu.Item key={`${path}/products`}>
            Products
          </Menu.Item>
          <Menu.Item key={`${path}/orders`}>
            Orders
          </Menu.Item>
        </Menu>
      </Layout.Sider>
      <Layout.Content>
        <Switch>
          <Route path={`${path}/products`}>
            <ProductManagementPage />
          </Route>
          <Route path={`${path}/orders`}>
            <OrderManagementPage />
          </Route>
          <Route path={`${path}`} exact>
            <DashboardPage />
          </Route>
          <Route>
            <NotFoundPage />
          </Route>
        </Switch>
      </Layout.Content>
    </Layout>
  )
}

export default AdminLayout
