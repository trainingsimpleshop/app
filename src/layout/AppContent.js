import React from 'react'
import { Switch, Route } from 'react-router-dom'
import NotFoundPage from '../page/NotFoundPage'
import AuthLayout from './AuthLayout'
import ProfilePage from '../page/ProfilePage'
import HomePage from '../page/HomePage'
import CartPage from '../page/CartPage'
import OrderDetailPage from '../page/OrderDetailPage'
import MyOrderPage from '../page/MyOrderPage'
import AdminLayout from './AdminLayout'

const AppContent = () => {
  return (
    <Switch>
      <Route path={["/signin", "/signup"]} exact>
        <AuthLayout />
      </Route>
      <Route path={["/profile"]} exact>
        <ProfilePage />
      </Route>
      <Route path="/" exact>
        <HomePage />
      </Route>
      <Route path="/cart" exact>
        <CartPage />
      </Route>
      <Route path="/order/:orderId" exact>
        <OrderDetailPage />
      </Route>
      <Route path="/myorders" exact>
        <MyOrderPage />
      </Route>
      <Route path="/admin">
        <AdminLayout />
      </Route>
      <Route>
        <NotFoundPage />
      </Route>
    </Switch>
  )
}

export default AppContent
