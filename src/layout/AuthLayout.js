import React from 'react'
import { Row, Col, Card } from 'antd'
import { Route, Switch } from 'react-router-dom'
import SignInPage from '../page/SignInPage'
import SignUpPage from '../page/SignUpPage'

const AuthLayout = () => {
  return (
    <Row justify="center">
      <Col>
        <Card style={{marginTop: 40}}>
          <Switch>
            <Route path="/signin" exact>
              <SignInPage />
            </Route>
            <Route path="/signup" exact>
              <SignUpPage />
            </Route>
          </Switch>
        </Card>
      </Col>
    </Row>
  )
}

export default AuthLayout
